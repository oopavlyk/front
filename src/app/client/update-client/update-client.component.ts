import {Component, Input, OnInit} from '@angular/core';
import {UpdateClientRequest} from "../dto/updateClientRequest";
import {ClientService} from "../client.service";
import {ActivatedRoute, Router} from "@angular/router";
import {HttpErrorResponse} from "@angular/common/http";
import {ModalDismissReasons, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ClientResponse} from "../dto/clientResponse";

@Component({
  selector: 'app-update-client',
  templateUrl: './update-client.component.html',
  styleUrls: ['./update-client.component.css']
})
export class UpdateClientComponent{

  client!: UpdateClientRequest;
  closeModal!: string;

  constructor(private clientService: ClientService,
              private route: ActivatedRoute,
              private router:Router,
              private modalService: NgbModal) {
    this.getClient()
  }

  getClient(){
    this.clientService.getClient(BigInt(Number(this.route.snapshot.paramMap.get('id')))).subscribe((response) =>{
      this.client = response;
      },
      (error: HttpErrorResponse) => {
      console.log(error);
      })
  }

  updateClient(){
    this.clientService.updateClient(this.client).subscribe(r =>{})
    this.modalService.dismissAll()
    this.reloadCurrentRoute()
  }

  reloadCurrentRoute() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
      this.router.navigate([currentUrl]);
    });
  }

  triggerModal(content: any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((res) => {
      this.closeModal = `Closed with: ${res}`;
    }, (res) => {
      this.closeModal = `Dismissed ${this.getDismissReason(res)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

}
