import {VehicleResponse} from "../../vehicle/dto/vehicleResponse";

export interface ClientResponse{
  id: bigint;
  firstName: string;
  lastName: string;
  birthday: Date;
  vehicles?: VehicleResponse[];
}
