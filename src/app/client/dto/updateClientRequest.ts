export interface UpdateClientRequest{
  id: bigint;
  firstName: string;
  lastName: string;
  birthday: Date;
  vehicleId?: bigint;
}
