export interface AddClientRequest{
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  birthday: Date;
}
