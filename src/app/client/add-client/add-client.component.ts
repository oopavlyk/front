import {Component, ElementRef, ViewChild} from '@angular/core';
import {ClientService} from "../client.service";
import {AddClientRequest} from "../dto/addClientRequest";
import {Router} from "@angular/router";
import {ModalDismissReasons, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {FormControl, Validators} from "@angular/forms";

@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.css']
})
export class AddClientComponent{

  first = new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(32)])
  last = new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(32)])
  email = new FormControl('', [Validators.required, Validators.email])
  password = new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(32)])
  birthday = new FormControl('', [Validators.required])

  client: AddClientRequest
  closeModal!: string;

  constructor(private clientService: ClientService,
              private router: Router,
              private modalService: NgbModal) {this.client = {
    birthday: new Date(),
    email: '',
    firstName: '',
    lastName: '',
    password: ''
  };
  }

  addClient() {
    this.client.firstName = this.first.value
    this.client.lastName = this.last.value
    this.client.email = this.email.value
    this.client.password = this.password.value
    this.clientService.addClient(this.client).subscribe(r=>{})
    this.modalService.dismissAll()
  }

  triggerModal(content: any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((res) => {
      this.closeModal = `Closed with: ${res}`;
    }, (res) => {
      this.closeModal = `Dismissed ${this.getDismissReason(res)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  /////////////////////////////////////   Validators
  getFirstErrorMessage() {
    if (this.first.hasError('required'))
      return 'You must enter a value';

    if (this.first.hasError('minlength'))
      return 'Minimum 2 signs';


    return this.first.hasError('maxlength') ? 'Maximum 32 signs' : '';
  }

  getLastErrorMessage() {
    if (this.last.hasError('required'))
      return 'You must enter a value';

    if (this.last.hasError('minlength'))
      return 'Minimum 2 signs';

    return this.last.hasError('maxlength') ? 'Maximum 32 signs' : '';
  }

  getPasswordErrorMessage() {
    if (this.password.hasError('required'))
      return 'You must enter a value';

    if (this.password.hasError('minlength'))
      return 'Minimum 2 signs';

    return this.password.hasError('maxlength') ? 'Maximum 32 signs' : '';
  }

  getEmailErrorMessage() {
    if (this.email.hasError('required'))
      return 'You must enter a value';

    return this.email.hasError('email') ? 'Not a valid email' : '';
  }

}
