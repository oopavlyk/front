import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {ClientResponse} from "./dto/clientResponse";
import {ClientService} from "./client.service";
import {HttpErrorResponse} from "@angular/common/http";
import {Router} from "@angular/router";
import {PageEvent} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {MatTableDataSource} from "@angular/material/table";
import {FormControl} from "@angular/forms";
import {debounceTime, switchMap} from "rxjs/operators";

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.scss']
})

export class ClientListComponent implements OnInit, AfterViewInit {

  public clients!: ClientResponse[];
  displayedColumns = ['id', 'firstName', 'lastName', 'birthday'];
  dataSource = new MatTableDataSource(this.clients)
  pageEvent: PageEvent = {
    pageIndex: 0,
    pageSize: 5,
    length: 0
  };
  filteredOptions: string[] | undefined
  search = new FormControl()
  backSort = new FormControl()
  parameter = new FormControl()
  page: number = this.pageEvent.pageIndex;


  @ViewChild(MatSort) sort!: MatSort;

  constructor(private clientService: ClientService,
              private router: Router) {
    this.backSort.setValue('.desc')
    this.parameter.setValue('id')
    this.search.setValue('')
  }

  ngOnInit() {
    this.change()
    this.getPage()
  }

  ngAfterViewInit(){
    this.dataSource.sort = this.sort
  }

  onClick(id: bigint){
    this.router.navigate(['/client', id])
  }

  change(){
    this.search.valueChanges.pipe(
        debounceTime(300),
        switchMap(value => this.clientService.search(value))
    )
        .subscribe((data:string[]) => {
          this.filteredOptions = data
          console.log(data)
        })
  }

  onNext(pageEvent: PageEvent){
    this.clientService.getPage(
      this.pageEvent.pageIndex,
      this.pageEvent.pageSize,
      this.parameter.value + this.backSort.value).subscribe(
      (response) => {
        this.page = this.pageEvent.pageIndex
        this.clients = response['content']
        this.dataSource = new MatTableDataSource(this.clients)
        this.ngAfterViewInit()
      },
      (error: HttpErrorResponse) => {
        console.log(error)
      }
    )
  }

  public getPage(){
    this.clientService.getPage(this.pageEvent.pageIndex, this.pageEvent.pageSize, this.parameter.value + this.backSort.value).subscribe(r=>{

      this.pageEvent.length = r['totalElements']
      this.pageEvent.pageSize = r['size']
      this.pageEvent.pageIndex = 0

      this.clients = r['content']
      this.dataSource = new MatTableDataSource(this.clients);
      this.ngAfterViewInit()
    }
    )
  }
}
