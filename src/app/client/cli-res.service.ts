import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from "@angular/router";
import {ClientService} from "./client.service";

@Injectable({
  providedIn: 'root'
})
export class CliResService implements Resolve<any>{

  constructor(private clientService: ClientService) { }

  resolve(route: ActivatedRouteSnapshot){
    let id = BigInt(Number(route.paramMap.get('id')));
    return this.clientService.getClient(id);
  }
}
