import {Component, Input, OnInit} from '@angular/core';
import {ClientService} from "../client.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-delete-client',
  templateUrl: './delete-client.component.html',
  styleUrls: ['./delete-client.component.css']
})
export class DeleteClientComponent implements OnInit {
  @Input()
  clientId: bigint = 0n;

  constructor(private clientService: ClientService,
              private router: Router) { }

  ngOnInit(): void {
  }

  onClick(){
    if(confirm("Are you sure to delete?")){
      this.deleteClient()
      this.router.navigate(['/list/client'])
    }
  }

  deleteClient(){
    this.clientService.deleteClient(this.clientId).subscribe(r=>{});
  }

}
