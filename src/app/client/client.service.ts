import { Injectable } from '@angular/core';
import {ClientResponse} from "./dto/clientResponse";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";
import {AddClientRequest} from "./dto/addClientRequest";
import {UpdateClientRequest} from "./dto/updateClientRequest";

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  apiServerUrl = environment.apiBaseUrl + '/client';

  constructor(private http: HttpClient) {
  }

  public getClients(): Observable<ClientResponse[]> {
    return this.http.get<ClientResponse[]>(this.apiServerUrl);
  }

  public getClient(id: bigint): Observable<ClientResponse>{
    return this.http.get<ClientResponse>(this.apiServerUrl + '/' + id);
  }

  public addClient(client: AddClientRequest){
    return this.http.post<AddClientRequest>(this.apiServerUrl, client);
  }

  public updateClient(client: UpdateClientRequest){
    return this.http.put<UpdateClientRequest>(this.apiServerUrl, client);
  }

  public deleteClient(id: bigint){
    return this.http.delete<void>(this.apiServerUrl + '/' + id);
  }

  public getPage(page: number, size: number, sort: string){
    return this.http.get<any>(this.apiServerUrl + '/paged?page=' + page + '&size=' + size + '&sort=' + sort)
  }

  public search(name: string){
    return this.http.get<string[]>(environment.apiBaseUrl + '/search/client?name=' + name)
  }
}
