import {Component} from '@angular/core';
import {ClientService} from "./client.service";
import {ClientResponse} from "./dto/clientResponse";
import {HttpErrorResponse} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";
import {VehicleService} from "../vehicle/vehicle.service";
import {VehicleResponse} from "../vehicle/dto/vehicleResponse";
import {UpdateClientRequest} from "./dto/updateClientRequest";
import {MatDialog} from "@angular/material/dialog";
import {AddInsuranceContractComponent} from "../insurance-contract/add-insurance-contract/add-insurance-contract.component";
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})

export class ClientComponent{
  client: ClientResponse;
  updateClient!: UpdateClientRequest;
  vehicles!: VehicleResponse[];
  vehicle = new FormControl()

  constructor(private clientService: ClientService,
              private vehicleService: VehicleService,
              private route: ActivatedRoute,
              private router: Router,
              public dialog: MatDialog) {
    this.client = this.route.snapshot.data.cliRes;
    this.getClient(BigInt(Number(this.route.snapshot.paramMap.get('id'))))
    this.getVehicles();
    this.updateClient = {
      id: this.client.id,
      firstName: this.client.firstName,
      lastName: this.client.lastName,
      birthday: this.client.birthday,
    }
  }

  public getClient(id: bigint) {
    this.clientService.getClient(id).subscribe(
      (response) => {
        this.client = response;
      },
      (error: HttpErrorResponse) => {
        console.log(error)
      }
    )
  }

  getVehicles(){
    this.vehicleService.getVehiclesForClients().subscribe(
      (response: VehicleResponse[]) => {
      this.vehicles = response;
    },
      (error: HttpErrorResponse) => {
      console.log(error)
      })
  }

  reloadCurrentRoute() {
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
      this.router.navigate([this.router.url]);
    });
  }

  addVehicleToClient(){
    this.updateClient.vehicleId = this.vehicle.value
    this.clientService.updateClient(this.updateClient).subscribe(r =>{})
    this.reloadCurrentRoute()
  }

  openDialog(){
    this.dialog.open(AddInsuranceContractComponent, {
      width: '300px',
      height: '350px',
      data: this.client.id
    }).afterClosed().subscribe(r => {
      this.reloadCurrentRoute()
    })
  }
}
