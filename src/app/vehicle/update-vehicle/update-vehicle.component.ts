import {Component, Inject} from '@angular/core';
import {UpdateVehicleRequest} from "../dto/updateVehicleRequest";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {FormControl, Validators} from "@angular/forms";

@Component({
  selector: 'app-update-vehicle',
  templateUrl: './update-vehicle.component.html',
  styleUrls: ['./update-vehicle.component.css']
})
export class UpdateVehicleComponent{
  color = new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(32)])
  engineSize = new FormControl('', [Validators.required, Validators.min(1.0), Validators.max(8.0)])
  weight = new FormControl('', [Validators.required])

  constructor(public dialogRef: MatDialogRef<UpdateVehicleComponent>,
              @Inject(MAT_DIALOG_DATA) public data: UpdateVehicleRequest) {
  }

  onNoClick(){
    this.dialogRef.close()
  }

  onOkClick(){
    this.data.color = this.color.value
    this.data.engineSize = this.engineSize.value
    this.data.weight = this.weight.value
    this.dialogRef.close(this.data)
  }

  getColorErrorMessage() {
    if (this.color.hasError('required'))
      return 'You must enter a value';

    if (this.color.hasError('minlength'))
      return 'Minimum 2 signs';

    return this.color.hasError('maxlength') ? 'Maximum 32 signs' : '';
  }

  getEngineSizeErrorMessage() {
    if (this.engineSize.hasError('required'))
      return 'You must enter a value';

    if (this.engineSize.hasError('min'))
      return 'Minimum value is 1.0 signs';

    return this.engineSize.hasError('max') ? 'Maximum 8.0 l' : '';
  }

  getWeightErrorMessage() {
    return this.engineSize.hasError('required')? 'You must enter a value': '';
  }

}
