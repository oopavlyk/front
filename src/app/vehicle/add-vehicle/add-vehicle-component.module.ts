import {NgModule} from '@angular/core';
import {AddVehicleComponent} from "./add-vehicle.component";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatDialogModule} from "@angular/material/dialog";
import {MatButtonModule} from "@angular/material/button";
import {ReactiveFormsModule} from "@angular/forms";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {BrowserModule} from "@angular/platform-browser";

@NgModule({
  declarations: [
    AddVehicleComponent
  ],
  imports: [
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    BrowserModule

  ],
  providers: []
})
export class AddVehicleComponentModule { }
