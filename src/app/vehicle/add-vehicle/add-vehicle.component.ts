import {Component} from '@angular/core';
import {VehicleService} from "../vehicle.service";
import {AddVehicleRequest} from "../dto/addVehicleRequest";
import {MatDialogRef} from "@angular/material/dialog";
import {FormControl, Validators} from "@angular/forms";

@Component({
  selector: 'app-add-vehicle',
  templateUrl: './add-vehicle.component.html',
  styleUrls: ['./add-vehicle.component.css']
})
export class AddVehicleComponent{
  color = new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(32)])
  engineSize = new FormControl('', [Validators.required, Validators.min(1.0), Validators.max(8.0)])
  weight = new FormControl('', [Validators.required])
  yearOfManufacturer = new FormControl([Validators.required])

  data: AddVehicleRequest = {
    color: '',
    engineSize: 0,
    yearOfManufacturer: new Date(),
    weight: 0
  }

  constructor(private vehicleService: VehicleService,
              public dialogRef: MatDialogRef<AddVehicleComponent>) {
  }

  onNoClick(): void {
    this.dialogRef.close()
  }

  onOkClick(){
    this.addVehicle()
    this.dialogRef.close()
  }

  addVehicle(){
    this.data.color = this.color.value
    this.data.weight = this.weight.value
    this.data.engineSize = this.engineSize.value
    this.data.yearOfManufacturer = this.yearOfManufacturer.value

    this.vehicleService.addVehicle(this.data).subscribe(r => {})
  }

  getColorErrorMessage() {
    if (this.color.hasError('required'))
      return 'You must enter a value';

    if (this.color.hasError('minlength'))
      return 'Minimum 2 signs';

    return this.color.hasError('maxlength') ? 'Maximum 32 signs' : '';
  }

  getEngineSizeErrorMessage() {
    if (this.engineSize.hasError('required'))
      return 'You must enter a value';

    if (this.engineSize.hasError('min'))
      return 'Minimum value is 1.0 signs';

    return this.engineSize.hasError('max') ? 'Maximum 8.0 l' : '';
  }

  getWeightErrorMessage() {
    return this.engineSize.hasError('required')? 'You must enter a value': '';
  }

  getYearOfManufacturerMessage() {
    return this.engineSize.hasError('required')? 'You must enter a value': '';
  }
  }
