import {Component} from '@angular/core';
import {VehicleResponse} from "./dto/vehicleResponse";
import {VehicleService} from "./vehicle.service";
import {ActivatedRoute} from "@angular/router";
import {HttpErrorResponse} from "@angular/common/http";
import {MatDialog} from "@angular/material/dialog";
import {UpdateVehicleComponent} from "./update-vehicle/update-vehicle.component";
import {UpdateVehicleRequest} from "./dto/updateVehicleRequest";

@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.css']
})
export class VehicleComponent{
  vehicle: VehicleResponse;
  show: boolean = false

  constructor(private vehicleService: VehicleService,
              private route: ActivatedRoute,
              public dialog: MatDialog) {
    this.vehicle = this.route.snapshot.data.vehRes;
    this.getVehicle();

  }

  getVehicle(){
    this.vehicleService.getVehicle(BigInt(Number(this.route.snapshot.paramMap.get('id')))).subscribe(
      (response => {
        this.vehicle = response
      }),
      (error: HttpErrorResponse) => {
        console.log(error);
      }
    )
    if (this.vehicle.client != null)
      this.show = true
  }

  updateVehicle(updateVehicleRequest: UpdateVehicleRequest){
    this.vehicleService.updateVehicle(updateVehicleRequest).subscribe(response =>{},
      (error: HttpErrorResponse) => {
      console.log(error)
      })
  }


  openDialog(): void {
    const dialogRef = this.dialog.open(UpdateVehicleComponent, {
      width: '300px',
      height: '350px',
      data: {
        id: this.vehicle.id,
        color: this.vehicle.color,
        engineSize: this.vehicle.engineSize,
        weight: this.vehicle.weight,
      }
    })

    dialogRef.afterClosed().subscribe(result => {
      if (result!=null)
        this.updateVehicle(result)
    })
  }



}
