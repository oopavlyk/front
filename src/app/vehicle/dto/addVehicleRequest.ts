export interface AddVehicleRequest{
  color: string;
  engineSize: number;
  weight: number;
  yearOfManufacturer: Date;
}
