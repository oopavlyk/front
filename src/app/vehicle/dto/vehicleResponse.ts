import {ClientResponse} from "../../client/dto/clientResponse";

export interface VehicleResponse{
  id: bigint;
  color: string;
  engineSize: number;
  yearOfManufacturer: Date;
  weight: number;
  client?: ClientResponse;
}
