export interface UpdateVehicleRequest{
  id: bigint;
  color: string;
  engineSize: number;
  weight: number;
}
