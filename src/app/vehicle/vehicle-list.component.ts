import {Component, OnInit} from '@angular/core';
import {VehicleResponse} from "./dto/vehicleResponse";
import {VehicleService} from "./vehicle.service";
import {HttpErrorResponse} from "@angular/common/http";
import {Router} from "@angular/router";
import {PageEvent} from "@angular/material/paginator";
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-vehicle-list',
  templateUrl: './vehicle-list.component.html',
  styleUrls: ['./vehicle-list.component.css']
})
export class VehicleListComponent implements OnInit {
  public vehicles!: VehicleResponse[];
  displayedColumns = ['id', 'color', 'engineSize', 'weight', 'yom'];
  pageEvent: PageEvent = {
    pageIndex: 0,
    pageSize: 5,
    length: 0
  };
  backSort = new FormControl()
  parameter = new FormControl()
  page: number = this.pageEvent.pageIndex;

  constructor(private vehicleService: VehicleService,
              private router: Router) {
    this.backSort.setValue('.desc')
    this.parameter.setValue('id')
  }

  ngOnInit(): void {
    this.getPage();
  }

  onClick(id: bigint) {
    this.router.navigate(['/vehicle', id]);
  }

  onNext(pageEvent: PageEvent){
    this.vehicleService.getCriteria(
      this.pageEvent.pageIndex,
      this.pageEvent.pageSize,
      this.parameter.value + this.backSort.value).subscribe(
      (response) => {
        this.page = this.pageEvent.pageIndex
        this.vehicles = response['content']

      },
      (error: HttpErrorResponse) => {
        console.log(error)
      }
    )
  }

  public getPage(){
    this.vehicleService.getCriteria(this.pageEvent.pageIndex, this.pageEvent.pageSize, this.parameter.value + this.backSort.value).subscribe(r=>{

        this.pageEvent.length = r['totalElements']
        this.pageEvent.pageSize = r['size']
        this.pageEvent.pageIndex = 0

        this.vehicles = r['content']
      }
    )
  }
}
