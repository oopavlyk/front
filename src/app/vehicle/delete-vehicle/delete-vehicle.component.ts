import {Component, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {VehicleService} from "../vehicle.service";

@Component({
  selector: 'app-delete-vehicle',
  templateUrl: './delete-vehicle.component.html',
  styleUrls: ['./delete-vehicle.component.css']
})
export class DeleteVehicleComponent implements OnInit {
  @Input()
  vehicleId: bigint = 0n;

  constructor(private vehicleService: VehicleService,
              private router: Router) { }

  ngOnInit(): void {
  }

  onClick(){
    if(confirm("Are you sure to delete?")){
      this.deleteVehicle()
      this.router.navigate(['/list/vehicle'])
    }
  }

  deleteVehicle(){
    this.vehicleService.deleteVehicle(this.vehicleId).subscribe(r=>{});
  }

}
