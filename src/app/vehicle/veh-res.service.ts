import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from "@angular/router";
import {VehicleService} from "./vehicle.service";

@Injectable({
  providedIn: 'root'
})
export class VehResService implements Resolve<any>{

  constructor(private vehicleService: VehicleService) { }

  resolve(route: ActivatedRouteSnapshot){
    let id = BigInt(Number(route.paramMap.get('id')));
    return this.vehicleService.getVehicle(id);
  }
}
