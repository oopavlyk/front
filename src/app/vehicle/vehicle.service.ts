import { Injectable} from "@angular/core";
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {VehicleResponse} from "./dto/vehicleResponse";
import {AddVehicleRequest} from "./dto/addVehicleRequest";
import {UpdateVehicleRequest} from "./dto/updateVehicleRequest";

@Injectable({
  providedIn: 'root'
})
export class VehicleService {
  apiServerUrl = environment.apiBaseUrl + '/vehicle';

  constructor(private http: HttpClient) {
  }

  public getCriteria(page: number, size: number, sort: string){
    return this.http.get<any>(this.apiServerUrl + '/criteria?page=' + page + '&size=' + size + '&sort=' + sort)
  }

  public getVehicles(): Observable<VehicleResponse[]>{
    return this.http.get<VehicleResponse[]>(this.apiServerUrl);
  }

  public getVehiclesForClients(): Observable<VehicleResponse[]>{
    return this.http.get<VehicleResponse[]>(this.apiServerUrl + '/free')
  }

  public getVehiclesForContract(clientId: bigint): Observable<VehicleResponse[]>{
    return this.http.get<VehicleResponse[]>(this.apiServerUrl + '/forContract/client/' + clientId)
  }

  public getVehicle(id: bigint): Observable<VehicleResponse>{
    return this.http.get<VehicleResponse>(this.apiServerUrl  + '/' + id);
  }

  public addVehicle(vehicle: AddVehicleRequest){
    return this.http.post<AddVehicleRequest>(this.apiServerUrl + '', vehicle);
  }

  public updateVehicle(vehicle: UpdateVehicleRequest) {
    return this.http.put<UpdateVehicleRequest>(this.apiServerUrl + '', vehicle);
  }

  public deleteVehicle(id: bigint) {
    return this.http.delete<void>(this.apiServerUrl + '/' + id);
  }
}
