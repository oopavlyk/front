import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from "@angular/common/http";

import {RouterModule, Routes} from "@angular/router";

import {AppComponent} from './app.component';
import {ClientListComponent} from "./client/client-list.component";
import {ClientComponent} from './client/client.component';
import {VehicleListComponent} from './vehicle/vehicle-list.component';
import {AddClientComponent} from './client/add-client/add-client.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AddVehicleComponent} from './vehicle/add-vehicle/add-vehicle.component';
import {VehicleComponent} from './vehicle/vehicle.component';
import {UpdateClientComponent} from './client/update-client/update-client.component';
import {DeleteClientComponent} from './client/delete-client/delete-client.component';
import {DeleteVehicleComponent} from './vehicle/delete-vehicle/delete-vehicle.component';
import {InsuranceContractComponent} from './insurance-contract/insurance-contract.component';
import {AddInsuranceContractComponent} from './insurance-contract/add-insurance-contract/add-insurance-contract.component';
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {VehResService} from "./vehicle/veh-res.service";
import {CliResService} from "./client/cli-res.service";
import {InsuranceContractClientComponent} from './insurance-contract/insurance-contract-client/insurance-contract-client.component';
import {KitCliResService} from "./insurance-contract/insurance-contract-client/kit-cli-res.service";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatSliderModule} from "@angular/material/slider";
import {MatTableModule} from "@angular/material/table";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatSelectModule} from "@angular/material/select";
import {MatInputModule} from "@angular/material/input";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatButtonModule} from "@angular/material/button";
import {MatNativeDateModule} from "@angular/material/core";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatSortModule} from "@angular/material/sort";
import {UpdateVehicleComponentModule} from "./vehicle/update-vehicle/update-vehicle-component.module";
import {AddVehicleComponentModule} from "./vehicle/add-vehicle/add-vehicle-component.module";
import {AddInsuranceContractComponentModule} from "./insurance-contract/add-insurance-contract/add-insurance-contract-component.module";
import {MatCardModule} from "@angular/material/card";
import {MatAutocompleteModule} from "@angular/material/autocomplete";


const appRoutes: Routes =[
  {path: '', component: AppComponent},
  {path: 'list/client', component: ClientListComponent},
  {path: 'client/:id',
    component: ClientComponent,
    resolve:{
      kitCliRes: KitCliResService,
      cliRes: CliResService
    }
  },
  {path: 'vehicle/:id',
    component: VehicleComponent,
    resolve: {
      //kitVehRes: KitVehResService,
      vehRes: VehResService
    }
  },
  {path: 'newClient', component: AddClientComponent},
  {path: 'client/:id/update', component: UpdateClientComponent},
  {path: 'list/vehicle', component: VehicleListComponent},
  {path: 'client/:clientId/newVehicle', component: AddVehicleComponent},

  {path: 'vehicle/:vehicleId/insuranceContract', component: AddInsuranceContractComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    ClientListComponent,
    ClientComponent,
    VehicleListComponent,
    AddClientComponent,
    VehicleComponent,
    UpdateClientComponent,
    DeleteClientComponent,
    DeleteVehicleComponent,
    InsuranceContractComponent,
    InsuranceContractClientComponent,
  ],
    imports: [
        BrowserModule,
        RouterModule.forRoot(appRoutes),
        HttpClientModule,
        FormsModule,
        MatSliderModule,
        NgbModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        MatTableModule,
        MatFormFieldModule,
        MatSelectModule,
        MatInputModule,
        MatDatepickerModule,
        MatButtonModule,
        MatNativeDateModule,
        MatPaginatorModule,
        MatSortModule,
        UpdateVehicleComponentModule,
        AddVehicleComponentModule,
        AddInsuranceContractComponentModule,
        MatCardModule,
        MatAutocompleteModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
