import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {InsuranceKitResponse} from "./dto/insuranceKitResponse";

@Injectable({
  providedIn: 'root'
})
export class InsuranceKitService {

  apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient) { }

  getList(): Observable<InsuranceKitResponse[]>{
    return this.http.get<InsuranceKitResponse[]>(this.apiServerUrl + '/insuranceKit')
  }
}
