export interface InsuranceKitResponse{
  id: bigint,
  duration: number;
  compensationPercent: number;
  damageLevel: string;
  coveredPart: string;
}
