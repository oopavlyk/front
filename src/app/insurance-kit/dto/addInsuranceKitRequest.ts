export interface AddInsuranceKitRequest{
  id: bigint,
  duration: number;
  compensationPercent: number;
  damageLevel: string;
  coveredPart: string;
}
