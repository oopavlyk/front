import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from "@angular/router";
import {InsuranceContractService} from "../insurance-contract.service";


@Injectable({
  providedIn: 'root'
})
export class KitCliResService implements Resolve<any>{

  constructor(private insuranceContractService: InsuranceContractService) { }

  resolve(route: ActivatedRouteSnapshot) {
    let id = BigInt(Number(route.paramMap.get('id')));
    return this.insuranceContractService.getInsuranceContractClient(id);
  }
}
