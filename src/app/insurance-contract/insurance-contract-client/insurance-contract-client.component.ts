import {Component, Input, OnInit} from '@angular/core';
import {InsuranceContractService} from "../insurance-contract.service";
import {InsuranceContractClientResponse} from "../dto/insuranceContractClientResponse";
import {HttpErrorResponse} from "@angular/common/http";
import {Router} from "@angular/router";
import {Observable} from "rxjs";

@Component({
  selector: 'app-insurance-contract-client',
  templateUrl: './insurance-contract-client.component.html',
  styleUrls: ['./insurance-contract-client.component.css']
})
export class InsuranceContractClientComponent implements OnInit {

  @Input()
  clientId: bigint = 0n;

  insuranceContracts: InsuranceContractClientResponse[] = [];

  constructor(private insuranceContractService: InsuranceContractService) {
  }

  async ngOnInit() {
    await this.fsf()
  }

  async fsf(){
    var response = await this.getInsuranceContract(this.clientId).toPromise()
    if(response){
      this.insuranceContracts = response
    }
  }

  getInsuranceContract(clientId: bigint): Observable<InsuranceContractClientResponse[]>{
     return this.insuranceContractService.getInsuranceContractClient(clientId)
  }
}
