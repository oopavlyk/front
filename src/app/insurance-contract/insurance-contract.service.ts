import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {InsuranceContractVehicleResponse} from "./dto/insuranceContractVehicleResponse";
import {InsuranceContractClientResponse} from "./dto/insuranceContractClientResponse";
import {AddInsuranceContractRequest} from "./dto/addInsuranceContractRequest";

@Injectable({
  providedIn: 'root'
})
export class InsuranceContractService {
  apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient) { }

  getInsuranceContractVehicle(vehicleId: bigint): Observable<InsuranceContractVehicleResponse>{
    return this.http.get<InsuranceContractVehicleResponse>(this.apiServerUrl + '/insuranceContract' + '/vehicle/' + vehicleId);
  }

  getInsuranceContractClient(clientId: bigint): Observable<InsuranceContractClientResponse[]> {
    return this.http.get<InsuranceContractClientResponse[]>(this.apiServerUrl + '/insuranceContract' + '/client/' + clientId);
  }

  create(addInsuranceContractRequest: AddInsuranceContractRequest){
    return this.http.post<AddInsuranceContractRequest>(this.apiServerUrl + '/insuranceContract',  addInsuranceContractRequest);
  }
}
