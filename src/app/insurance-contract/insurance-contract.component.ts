import {Component, Input, OnInit} from '@angular/core';
import {InsuranceContractService} from "./insurance-contract.service";
import {InsuranceContractVehicleResponse} from "./dto/insuranceContractVehicleResponse";
import {Observable} from "rxjs";

@Component({
  selector: 'app-insurance-contract',
  templateUrl: './insurance-contract.component.html',
  styleUrls: ['./insurance-contract.component.css']
})
export class InsuranceContractComponent implements OnInit{
  @Input()
  vehicleId: bigint = 0n;

  show: boolean = false;
  insuranceContract!: InsuranceContractVehicleResponse;

  constructor(private insuranceContractService: InsuranceContractService) {
  }

  async ngOnInit() {
    await this.get()
  }

  async get(){
    var response = await this.getInsuranceContract().toPromise()
    if (response) {
      this.insuranceContract = response
      this.show = true
    }
  }

  getInsuranceContract(): Observable<InsuranceContractVehicleResponse> {
    return this.insuranceContractService.getInsuranceContractVehicle(this.vehicleId)
  }

}
