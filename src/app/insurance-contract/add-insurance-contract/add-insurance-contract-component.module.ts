import {NgModule} from '@angular/core';
import {AddInsuranceContractComponent} from "./add-insurance-contract.component";
import {MatDialogModule} from "@angular/material/dialog";
import {MatInputModule} from "@angular/material/input";
import {ReactiveFormsModule} from "@angular/forms";
import {BrowserModule} from "@angular/platform-browser";
import {MatButtonModule} from "@angular/material/button";
import {MatSelectModule} from "@angular/material/select";

@NgModule({
  declarations: [
    AddInsuranceContractComponent
  ],
  imports: [
    MatDialogModule,
    MatInputModule,
    ReactiveFormsModule,
    BrowserModule,
    MatButtonModule,
    MatSelectModule

  ],
  providers: []
})
export class AddInsuranceContractComponentModule { }
