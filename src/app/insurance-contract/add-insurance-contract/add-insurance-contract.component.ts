import {Component, Inject} from '@angular/core';
import {InsuranceContractService} from "../insurance-contract.service";
import {ActivatedRoute, Router} from "@angular/router";
import {ModalDismissReasons, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {InsuranceKitService} from "../../insurance-kit/insurance-kit.service";
import {InsuranceKitResponse} from "../../insurance-kit/dto/insuranceKitResponse";
import {HttpErrorResponse} from "@angular/common/http";
import {AddInsuranceContractRequest} from "../dto/addInsuranceContractRequest";
import {FormControl, Validators} from "@angular/forms";
import {VehicleResponse} from "../../vehicle/dto/vehicleResponse";
import {VehicleService} from "../../vehicle/vehicle.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {UpdateVehicleRequest} from "../../vehicle/dto/updateVehicleRequest";

@Component({
  selector: 'app-add-insurance-contract',
  templateUrl: './add-insurance-contract.component.html',
  styleUrls: ['./add-insurance-contract.component.css']
})
export class AddInsuranceContractComponent {
  insuranceKits = new FormControl('', [Validators.required])
  vehicle = new FormControl([Validators.required])

  insuranceKitList: InsuranceKitResponse[] = [];
  addInsuranceContractRequest: AddInsuranceContractRequest;
  vehicles: VehicleResponse[] = [];

  constructor(private insuranceContractService: InsuranceContractService,
              private insuranceKitService: InsuranceKitService,
              private vehicleService: VehicleService,
              private route: ActivatedRoute,
              public dialogRef: MatDialogRef<AddInsuranceContractComponent>,
              @Inject(MAT_DIALOG_DATA) public clientId: number) {
    this.addInsuranceContractRequest = {
      insuranceKit: [],
      vehicleId: 0,
      clientId: clientId
    };
    this.getinsuranceKitList();
    this.getVehicles();
  }

  onNoClick(){
    this.dialogRef.close()
  }

  onOkClick(){
    this.createInsuranceContract()
    this.dialogRef.close()
  }

  getinsuranceKitList(){
    this.insuranceKitService.getList().subscribe(request =>{
      this.insuranceKitList = request
    },
      (error: HttpErrorResponse) =>{
      console.log(error)
      })
  }

  getVehicles(){
    this.vehicleService.getVehiclesForContract(BigInt(Number(this.route.snapshot.paramMap.get('id')))).subscribe(
      (response: VehicleResponse[]) => {
        this.vehicles = response;
      },
      (error: HttpErrorResponse) => {
        console.log(error)
      })
  }

  createInsuranceContract(){
    this.addInsuranceContractRequest.insuranceKit = this.insuranceKits.value
    this.addInsuranceContractRequest.vehicleId = this.vehicle.value

    this.insuranceContractService.create(this.addInsuranceContractRequest).subscribe(r => {})
  }

  getInsuranceKitsErrorMessage() {
    return this.insuranceKits.hasError('required')? 'You must enter a value': '';
  }

  getVehicleErrorMessage() {
    return this.vehicle.hasError('required')? 'You must enter a value': '';
  }

}
