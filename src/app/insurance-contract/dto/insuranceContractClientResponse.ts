import {VehicleResponse} from "../../vehicle/dto/vehicleResponse";
import {InsuranceKitResponse} from "../../insurance-kit/dto/insuranceKitResponse";

export interface InsuranceContractClientResponse{
  vehicleClientResponse: VehicleResponse,
  insuranceKit: InsuranceKitResponse[];
}
