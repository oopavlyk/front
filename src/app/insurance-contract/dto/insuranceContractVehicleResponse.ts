import {InsuranceKitResponse} from "../../insurance-kit/dto/insuranceKitResponse";
import {ClientResponse} from "../../client/dto/clientResponse";

export interface InsuranceContractVehicleResponse{
  clientVehicleResponse: ClientResponse,
  insuranceKit: InsuranceKitResponse[];
}
