export interface InsuranceKit{
  duration: number;
  compensationPercent: number;
  damageLevel: string;
  coveredPart: string;
}
