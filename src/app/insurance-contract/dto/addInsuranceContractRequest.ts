import {AddInsuranceKitRequest} from "../../insurance-kit/dto/addInsuranceKitRequest";

export interface AddInsuranceContractRequest{
  clientId: number,
  vehicleId: number,
  insuranceKit: AddInsuranceKitRequest[]
}
