import {Component} from '@angular/core';
import {Router} from "@angular/router";
import {UpdateVehicleComponent} from "./vehicle/update-vehicle/update-vehicle.component";
import {MatDialog} from "@angular/material/dialog";
import {AddVehicleComponent} from "./vehicle/add-vehicle/add-vehicle.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {


  title: string;

  constructor(private router: Router,
              public dialog: MatDialog) {
    this.title = 'Spring Boot - Angular Application';
    this.router.navigate(['/list/client']);
  }

  openDialog(): void {
    this.dialog.open(AddVehicleComponent, {
      width: '300px',
      height: '430px',
    })
  }
}
